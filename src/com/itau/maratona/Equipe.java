package com.itau.maratona;

import java.util.ArrayList;
import java.util.List;

public class Equipe {
	public int idEquipe;
	public List<Aluno> alunos;
	
	public Equipe () {
		alunos = new ArrayList<>();
		
	}

	public String toString() {
			String texto = Integer.toString(idEquipe);
			
			for (Aluno aluno : alunos) {
				texto += aluno.nome;
				texto += "\n";

			}
            return texto;
	
	}

}

